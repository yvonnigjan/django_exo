from django.urls import path, re_path
from todo.views import index, itineraire_list, split_voie, fast_itineraire_list


urlpatterns = [
    path('', index, name='index'),
    path('itineraire_list/', itineraire_list, name='itineraire_list'),
    path('fast_itineraire_list/', fast_itineraire_list, name='fast_itineraire_list'),
    re_path(r'^split_voie/(?P<voie_id>[0-9]+)/$', split_voie, name='split_voie'),
]
