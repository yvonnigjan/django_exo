from todo.models import Voie, Signal, Ligne
from django.shortcuts import get_object_or_404
import datetime
import pytz


def _split_voie(voie_id, new_date, new_pk, libelle_1, libelle_2):
    # Old Voie
    old_voie = get_object_or_404(Voie, pk=voie_id)
    old_voie.dateFinActivite = new_date
    old_voie.save()

    # New Voie 1
    new_voie_1 = Voie.objects.create(
        parent=old_voie.parent,
        dateDebutActivite=new_date,
        dateFinActivite=datetime.datetime(2200, 1, 1, 0, 0, tzinfo=pytz.utc),
        pk_debut=old_voie.pk_debut,
        pk_fin=new_pk,
        libelle=libelle_1
    )
    new_voie_1.save()

    # New Voie 2
    new_voie_2 = Voie.objects.create(
        parent=old_voie.parent,
        dateDebutActivite=new_date,
        dateFinActivite=datetime.datetime(2200, 1, 1, 0, 0, tzinfo=pytz.utc),
        pk_debut=new_pk,
        pk_fin=old_voie.pk_fin,
        libelle=libelle_2
    )
    new_voie_2.save()

    # Update Signals voies
    for signal in old_voie.Voie_rn.filter(point_kilometrique__lte=new_pk):
        signal.voie.remove(old_voie)
        signal.voie.add(new_voie_1)
        signal.save()

    for signal in old_voie.Voie_rn.filter(point_kilometrique__gt=new_pk):
        signal.voie.remove(old_voie)
        signal.voie.add(new_voie_2)
        signal.save()
    return new_voie_1, new_voie_2
