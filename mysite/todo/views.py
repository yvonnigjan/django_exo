from django.http import HttpResponse, HttpResponseNotFound
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from todo.models import Itineraire, Signal
from todo.serializers import ItineraireSerializer, VoieSerializer
from django.utils import timezone
from django.db.models import Q
from todo.utils import _split_voie
from django.utils.dateparse import parse_datetime
import pytz
from django.db import connection


def index(request):
    itis = Itineraire.objects.all()
    return HttpResponse(itis.first().libelle)


@api_view(['GET'])
def itineraire_list(request):
    now = timezone.now()
    libelle = request.GET.get("libelle")
    if libelle is None:
        return HttpResponseNotFound("Libelle Parameter not found.")

    itis = Itineraire.objects.filter(
        Q(signal_debut__libelle__iexact=libelle,
          signal_debut__dateDebutActivite__lt=now)
        | Q(signal_fin__libelle__iexact=libelle, signal_debut__dateFinActivite__gt=now))

    serializer = ItineraireSerializer(itis, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def fast_itineraire_list(request):

    now = timezone.now()
    str_now = now.strftime("%Y-%m-%d %H:%M:%S")
    libelle = request.GET.get("libelle")
    if libelle is None:
        return HttpResponseNotFound("Libelle Parameter not found.")
    query = f"""
        SELECT  
                ti.ressourceabstraite_ptr_id
        FROM todo_itineraire ti 
        INNER JOIN
            (
                select ressourceabstraite_ptr_id as pk, libelle, dateDebutActivite, dateFinActivite, point_kilometrique
                from todo_signal ts inner join todo_ressourceabstraite tr 
                on ts.ressourceabstraite_ptr_id=tr.id
                WHERE lower(tr.libelle) like lower('{libelle}') AND
                    dateDebutActivite < datetime('{str_now}')
                    and dateFinActivite > datetime('{str_now}')
                ) s1
            on ti.signal_debut_id=s1.pk 
            or ti.signal_fin_id=s1.pk
        ;
        """
    itis = Itineraire.objects.raw(query)
    serializer = ItineraireSerializer(itis, many=True)
    return Response(serializer.data)


@api_view(["POST"])
def split_voie(request, voie_id):

    new_date = parse_datetime(request.POST.get("date"))
    new_date.replace(tzinfo=pytz.utc)
    new_pk = request.POST.get("pk")
    libelle_1 = request.POST.get("libelle_1")
    libelle_2 = request.POST.get("libelle_2")
    new_voie_1, new_voie_2 = _split_voie(
        voie_id, new_date, new_pk, libelle_1, libelle_2)
    serializer = VoieSerializer([new_voie_1, new_voie_2], many=True)

    return Response(serializer.data)
