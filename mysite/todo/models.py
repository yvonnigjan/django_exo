from django.db import models

class Ressourceabstraite(models.Model):
    libelle = models.TextField(null=True, verbose_name='Libelle')
    dateDebutActivite = models.DateTimeField(null=True, verbose_name='Date Debut Activite')
    dateFinActivite = models.DateTimeField(null=True, verbose_name='Date Fin Activite')


class ElementLineaire(Ressourceabstraite):
    pk_debut = models.IntegerField(null=True, verbose_name='PK Début')
    pk_fin = models.IntegerField(null=True, verbose_name='PK Fin')


class Signal(Ressourceabstraite):
    point_kilometrique = models.IntegerField(null=True, verbose_name='PK')
    voie = models.ManyToManyField('Voie', db_constraint=False, related_name='Voie_rn')

    
class Voie(ElementLineaire):
    parent = models.ForeignKey('Ligne', on_delete=models.CASCADE, db_constraint=False, related_name='Ligne_rn', null=True)


class Ligne(ElementLineaire):
    pass


class Itineraire(Ressourceabstraite):
    signal_debut = models.ForeignKey('Signal', on_delete=models.CASCADE, db_constraint=False, related_name='Signal_debut_rn', null=True)
    signal_fin = models.ForeignKey('Signal', on_delete=models.CASCADE, db_constraint=False, related_name='Signal_fin_rn', null=True)
    
