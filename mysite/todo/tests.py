from django.test import TestCase
from todo.models import Voie, Signal, Ligne
import datetime
from todo.utils import _split_voie
import pytz
# Create your tests here.

MIN_DATE = datetime.datetime(1800, 1, 1, 0, 0, tzinfo=pytz.utc)
MAX_DATE = datetime.datetime(2200, 1, 1, 0, 0, tzinfo=pytz.utc)
NEW_DATE = datetime.datetime(2022, 1, 1, 0, 0, tzinfo=pytz.utc)
NEW_PK = 100


class UtilsTestCase(TestCase):
    def setUp(self):
        line = Ligne.objects.create(pk=99, libelle="lille-marseille", pk_debut=0,
                                    pk_fin=623, dateDebutActivite=MIN_DATE, dateFinActivite=MAX_DATE)
        line.save()
        old_voie = Voie.objects.create(pk=99, libelle="lille-paris", pk_debut=0,
                                       pk_fin=200, dateDebutActivite=MIN_DATE, dateFinActivite=MAX_DATE, parent=line)
        old_voie.save()
        signal_1 = Signal(pk=99,  libelle="C90", dateFinActivite=MAX_DATE,
                          dateDebutActivite=MIN_DATE, point_kilometrique=25)
        signal_1.save()
        signal_1.voie.add(old_voie)
        signal_1.save()
        signal_2 = Signal(pk=100, libelle="C91", dateFinActivite=MAX_DATE,
                          dateDebutActivite=MIN_DATE, point_kilometrique=150)
        signal_2.save()
        signal_2.voie.add(old_voie)
        signal_2.save()

    def test_split(self):
        _split_voie(voie_id=99, new_date=NEW_DATE, new_pk=NEW_PK,
                    libelle_1="lille-amiens", libelle_2="amiens-paris")
        l = Ligne.objects.get(pk=99)
        voies = l.Ligne_rn.all()
        print(voies)
        old_voie = voies.get(pk=99)
        new_voie_1 = voies.get(pk_fin=NEW_PK)
        new_voie_2 = voies.get(pk_debut=NEW_PK)

        self.assertEqual(voies.all().count(), 3)
        self.assertEqual(old_voie.dateFinActivite, NEW_DATE)

        self.assertEqual(new_voie_1.dateDebutActivite, NEW_DATE)
        self.assertEqual(new_voie_1.dateFinActivite, MAX_DATE)
        self.assertEqual(new_voie_1.pk_debut, 0)
        self.assertEqual(new_voie_1.Voie_rn.first().libelle, "C90")

        self.assertEqual(new_voie_2.dateDebutActivite, NEW_DATE)
        self.assertEqual(new_voie_2.dateFinActivite, MAX_DATE)
        self.assertEqual(new_voie_2.pk_fin, 200)
        self.assertEqual(new_voie_2.Voie_rn.first().libelle, "C91")
