from todo.models import Itineraire, Signal, Voie
from rest_framework import serializers


class VoieSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Voie
        fields = ['pk', 'libelle', 'pk_debut', 'pk_fin',
                  'dateDebutActivite', 'dateFinActivite']


class SignalSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Signal
        fields = ['pk', 'libelle', 'dateDebutActivite',
                  'dateFinActivite', 'point_kilometrique']


class ItineraireSerializer(serializers.HyperlinkedModelSerializer):
    signal_debut = SignalSerializer(many=False, read_only=False)
    signal_fin = SignalSerializer(many=False, read_only=False)

    class Meta:
        model = Itineraire
        fields = ['pk', 'signal_debut', 'signal_fin']
